package com.example.dewo.controller;

import com.example.dewo.dto.DtoUser;
import com.example.dewo.exception.ResourceNotFoundException;
import com.example.dewo.model.User;
import com.example.dewo.payload.Response;
import com.example.dewo.repository.UserRepository;
import com.example.dewo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/api/users")
@RestController
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody DtoUser dtoUser) {
        return ResponseEntity.ok(userService.createDataUser(dtoUser));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@PathVariable(value = "id") Long id,
                                        @Valid @RequestBody DtoUser dtoUser) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
        return ResponseEntity.ok(userService.updateUser(user, dtoUser));
    }
}
