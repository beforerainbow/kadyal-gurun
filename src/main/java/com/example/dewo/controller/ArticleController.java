package com.example.dewo.controller;

import com.example.dewo.dto.DtoArticle;
import com.example.dewo.dto.DtoData;
import com.example.dewo.dto.DtoTest;
import com.example.dewo.dto.DtoUser;
import com.example.dewo.exception.ResourceNotFoundException;
import com.example.dewo.model.Article;
import com.example.dewo.model.User;
import com.example.dewo.payload.Response;
import com.example.dewo.payload.ResponseUser;
import com.example.dewo.repository.ArticleRepository;
import com.example.dewo.repository.UserRepository;
import com.example.dewo.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RequestMapping("api/user/")
@RestController
public class ArticleController {

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ArticleService articleService;

    @PostMapping("/{id}/article/")
    public ResponseEntity<?> createArticle(@Valid @RequestBody DtoArticle dtoArticle,
                                           @PathVariable(value = "id") Long id) {
        Article article = articleService.createArticle(dtoArticle, id);
        return ResponseEntity.ok(new Response(article));
    }


    @PutMapping("/{id}/article/")
    public ResponseEntity<?> updateArticle(@Valid @RequestBody DtoArticle dtoArticle,
                                           @PathVariable(value = "id") Long id) {
        Article article = articleRepository.findByUserId(id);
        Article updatedArticle = articleService.updateArticle(article, dtoArticle, id);
        return ResponseEntity.ok(new Response("article was updated", updatedArticle));
    }


    //    delete several article with related userId
    @DeleteMapping("/{idUser}/article/")
    public ResponseEntity<?> deleteArticle(@PathVariable(value = "idUser") Long id) {
        List<Article> article = articleRepository.optionalByUserId(id);
        articleService.deleteSeveralArticle(article);
        return ResponseEntity.ok("article data was delete");
    }


    //    Get User with detail article?/////
    @GetMapping("/getAll/users")
    public ResponseEntity<?> getUsersWith() {
        List<User> users = userRepository.findAll();
        List rowData = articleService.getAllUSer(users);
        return ResponseEntity.ok(new Response(rowData));
    }


    //    Get Article with detail user have it--------
    @GetMapping("/getAll/article")
    public ResponseEntity<?> getArticleWith() {
        List<Article> articles = articleRepository.findAll();
        List rowData = articleService.getAllArticle(articles);
        return ResponseEntity.ok(new Response(rowData));
    }


    @GetMapping("/{userId}/article")
    public ResponseEntity<?> getSinggleArticle(@PathVariable(value = "userId") Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("user", "userId", userId));
        List<Article> articles = articleRepository.optionalByUserId(userId);
        DtoUser dtoUser = new DtoUser(user);

        return ResponseEntity.ok(new ResponseUser(dtoUser, articles));
    }


    @GetMapping("/{userId}/articles")
    public ResponseEntity<?> getSeveralArticle(@PathVariable(value = "userId") Long userId) {
        Map<String, Object> data = articleService.getSeveralArticle(userId);
        return ResponseEntity.ok(new Response(data));
    }

    @GetMapping("/article/alias")
    public ResponseEntity<?> getWithAlias() {
        List<Object> data = articleRepository.listWithAlias();
        return ResponseEntity.ok(data);
    }

}
