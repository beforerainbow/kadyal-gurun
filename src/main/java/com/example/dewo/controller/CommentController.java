package com.example.dewo.controller;


import com.example.dewo.dto.DtoArticle;
import com.example.dewo.dto.DtoComment;
import com.example.dewo.dto.DtoData;
import com.example.dewo.dto.DtoUser;
import com.example.dewo.exception.ResourceNotFoundException;
import com.example.dewo.model.Article;
import com.example.dewo.model.Comment;
import com.example.dewo.model.User;
import com.example.dewo.payload.Response;
import com.example.dewo.payload.ResponseComment;
import com.example.dewo.repository.ArticleRepository;
import com.example.dewo.repository.CommentRepository;
import com.example.dewo.repository.UserRepository;
import com.example.dewo.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/api")
@RestController
public class CommentController {

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    CommentService commentService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ArticleRepository articleRepository;

    @PostMapping("/article/{articleId}/comment")
    public ResponseEntity<?> createComment(@RequestBody DtoComment dtoComment,
                                           @PathVariable(value = "articleId") Long id) {
        Comment comment = commentService.createComment(dtoComment, id);
        return ResponseEntity.ok(new Response(comment));
    }

    @PutMapping("/article/{articleId}/comment/{cmmntId}")
    public ResponseEntity<?> updateComment(@Valid @RequestBody DtoComment dtoComment,
                                           @PathVariable(value = "articleId") Long articleId,
                                           @PathVariable(value = "cmmntId") Long idCmnt) {
        DtoData dtoData = commentService.updateComment(dtoComment, articleId, idCmnt);
        return ResponseEntity.ok(new Response(dtoData));
    }


    //    get singgle comment with comment id
    @GetMapping("/article/comment/{cmntId}")
    public ResponseEntity<?> getDetailComment(@PathVariable(value = "cmntId") Long cmntId) {

        Comment comment = commentRepository.findById(cmntId)
                .orElseThrow(() -> new ResourceNotFoundException("comment", "id", cmntId));
        Article article = articleRepository.findById(comment.getArticleId())
                .orElseThrow(() -> new ResourceNotFoundException("article", "id", comment.getArticleId()));

        DtoComment dtoComment = new DtoComment(comment);
        DtoArticle dtoArticle = new DtoArticle(article);
        return ResponseEntity.ok(new ResponseComment(dtoArticle, dtoComment));
    }

    //    get list comment related with id article and singgle user in command
    @GetMapping("/article/{articleId}/comment")
    public ResponseEntity<?> getListComment(@PathVariable(value = "articleId") Long articleId) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new ResourceNotFoundException("article", "id", articleId));
        User user = userRepository.findById(article.getUserId())
                .orElseThrow(() -> new ResourceNotFoundException("user", "id", article.getUserId()));
        List<Comment> comments = commentRepository.optionalByArticleId(article.getId());
        List<Object> kerupuk = new ArrayList<>();
        DtoUser dtoUser = new DtoUser(user);
        for (int i = 0; i < comments.size(); i++) {
            Comment rowComment = comments.get(i);
            Map<String, Object> rambak = new HashMap<>();
            rambak.put("comment", rowComment);
            rambak.put("user", dtoUser);
            kerupuk.add(rambak);
        }
        DtoArticle dtoArticle = new DtoArticle(article, dtoUser);
        return ResponseEntity.ok(new ResponseComment(dtoArticle, kerupuk));
    }

    //    get list comment with user who have comment in payload
    @GetMapping("/article/{articleId}/comments")
    public ResponseEntity<?> getListCommentWithDetailUser(@PathVariable(value = "articleId") Long articleId) {
        Map<String, Object> data = commentService.getListComment(articleId);
        return ResponseEntity.ok(new Response(data));
    }
}
