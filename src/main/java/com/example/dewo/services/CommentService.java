package com.example.dewo.services;

import com.example.dewo.dto.DtoComment;
import com.example.dewo.dto.DtoData;
import com.example.dewo.model.Comment;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public interface CommentService {
    Comment createComment(DtoComment dtoComment, Long articleId);

    DtoData updateComment(DtoComment dtoComment, Long articleId, Long comntId);

    Map<String, Object> getListComment(Long articleId);
}
