package com.example.dewo.services;

import com.example.dewo.dto.DtoArticle;
import com.example.dewo.model.Article;
import com.example.dewo.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface ArticleService {
    Article createArticle(DtoArticle dtoArticle, Long idUser);

    Article updateArticle(Article article, DtoArticle dtoArticle, Long idUser);

    Article deleteSeveralArticle(List<Article> articleList);

    List getAllUSer(List<User> users);

    List getAllArticle(List<Article> articles);

    Map<String, Object> getSeveralArticle(Long userId);

}
