package com.example.dewo.services;

import com.example.dewo.dto.DtoComment;
import com.example.dewo.dto.DtoData;
import com.example.dewo.dto.DtoUser;
import com.example.dewo.exception.ResourceNotFoundException;
import com.example.dewo.model.Article;
import com.example.dewo.model.Comment;
import com.example.dewo.model.User;
import com.example.dewo.repository.ArticleRepository;
import com.example.dewo.repository.CommentRepository;
import com.example.dewo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Comment createComment(DtoComment dtoComment, Long articleId) {
        Comment comment = new Comment();
        comment.setArticleId(articleId);
        comment.setUserId(dtoComment.getUserId());
        comment.setFieldComment(dtoComment.getFieldComment());
        commentRepository.save(comment);
        return comment;
    }

    //update commant can update article id and user id
    @Override
    public DtoData updateComment(DtoComment dtoComment, Long articleId, Long comntId) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new ResourceNotFoundException("article", "id", articleId));
        Comment comment = commentRepository.findById(comntId)
                .orElseThrow(() -> new ResourceNotFoundException("comment", "id", comntId));
        comment.setFieldComment(dtoComment.getFieldComment());
        comment.setUserId(dtoComment.getUserId());
        comment.setArticleId(articleId);

        Comment updateComment = commentRepository.save(comment);
        DtoData dtoData = new DtoData(article, updateComment);
        return dtoData;
    }

    @Override
    public Map<String, Object> getListComment(Long articleId) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new ResourceNotFoundException("article", "id", articleId));
        List<Comment> comments = commentRepository.optionalByArticleId(article.getId());
        List<Object> commentList = new ArrayList<>();
        for (int i = 0; i < comments.size(); i++) {
            Comment rowComment = comments.get(i);
            User user = userRepository.findById(rowComment.getUserId())
                    .orElseThrow(() -> new ResourceNotFoundException("user", "id", rowComment.getUserId()));
            DtoUser dtoUser = new DtoUser(user);
            DtoComment dtoComment = new DtoComment(rowComment, dtoUser);
            commentList.add(dtoComment);
        }
        Map<String, Object> data = new HashMap<>();
        data.put("article", article);
        data.put("comment", commentList);
        return data;
    }
}
