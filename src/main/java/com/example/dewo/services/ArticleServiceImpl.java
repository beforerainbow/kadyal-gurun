package com.example.dewo.services;

import com.example.dewo.dto.DtoArticle;
import com.example.dewo.dto.DtoData;
import com.example.dewo.dto.DtoUser;
import com.example.dewo.exception.ResourceNotFoundException;
import com.example.dewo.model.Article;
import com.example.dewo.model.User;
import com.example.dewo.repository.ArticleRepository;
import com.example.dewo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Article createArticle(DtoArticle dtoArticle, Long idUser) {
        Article article = new Article();
        article.setTitle(dtoArticle.getTitle());
        article.setDescription(dtoArticle.getTitle());
        article.setUserId(idUser);
        articleRepository.save(article);
        return article;
    }


    @Override
    public Article updateArticle(Article article, DtoArticle dtoArticle, Long idUser) {
        article.setTitle(dtoArticle.getTitle());
        article.setDescription(dtoArticle.getTitle());
        article.setUserId(idUser);
        articleRepository.save(article);
        return article;
    }

    @Override
    public Article deleteSeveralArticle(List<Article> articleList) {
        articleList.forEach(article -> articleRepository.delete(article));
        return null;
    }


    @Override
    public List getAllUSer(List<User> users) {
        ArrayList rowData = new ArrayList();

        for (int i = 0; i < users.size(); i++) {
            User rowUser = users.get(i);
            List<Article> articles = articleRepository.optionalByUserId(rowUser.getId());
            Map<String, Object> detailArticle = new HashMap<>();
            detailArticle.put("article", articles);
            detailArticle.put("user", rowUser);
            rowData.add(detailArticle);
        }
        return rowData;
    }


    @Override
    public List getAllArticle(List<Article> articles) {
        ArrayList<Object> rowData = new ArrayList();

        for (int i = 0; i < articles.size(); i++) {
            Article rowArticle = articles.get(i);
            User user = userRepository.findById(rowArticle.getUserId())
                    .orElseThrow(() -> new ResourceNotFoundException("User", "id", rowArticle.getUserId()));
            DtoUser dtoUser = new DtoUser(user);
            DtoData dtoData = new DtoData(rowArticle, dtoUser);
            rowData.add(dtoData);
        }
        return rowData;
    }

    @Override
    public Map<String, Object> getSeveralArticle(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("user", "id", userId));
        List<Article> articles = articleRepository.optionalByUserId(userId);
        DtoUser dtoUser = new DtoUser(user, articles);
        Map<String, Object> data = new HashMap<>();
        data.put("user", dtoUser);
        return data;
    }
}
