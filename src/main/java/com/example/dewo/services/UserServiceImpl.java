package com.example.dewo.services;

import com.example.dewo.dto.DtoUser;
import com.example.dewo.model.User;
import com.example.dewo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User createDataUser(DtoUser dtoUser) {

        User user = new User();
        user.setFirstName(dtoUser.getFirstName());
        user.setLastName(dtoUser.getLastName());
        user.setAddress(dtoUser.getAddress());
        user.setNoHp(dtoUser.getNoHp());
        userRepository.save(user);
        return user;
    }

    @Override
    public User updateUser(User user, DtoUser dtoUser) {
        user.setFirstName(dtoUser.getFirstName());
        user.setLastName(dtoUser.getLastName());
        user.setAddress(dtoUser.getAddress());
        user.setNoHp(dtoUser.getNoHp());
        userRepository.save(user);
        return user;
    }
}
