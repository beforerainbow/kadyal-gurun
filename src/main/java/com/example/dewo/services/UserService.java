package com.example.dewo.services;

import com.example.dewo.dto.DtoUser;
import com.example.dewo.model.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    User createDataUser(DtoUser dtoUser);

    User updateUser(User user, DtoUser dtoUser);
}
