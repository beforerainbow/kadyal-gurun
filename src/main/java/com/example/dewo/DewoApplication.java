package com.example.dewo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DewoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DewoApplication.class, args);
	}

}
