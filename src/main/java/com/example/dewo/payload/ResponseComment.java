package com.example.dewo.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseComment {
    private Object article;
    private Object comment;

    public ResponseComment() {
    }

    public ResponseComment(Object article, Object comment) {
        this.article = article;
        this.comment = comment;
    }

}
