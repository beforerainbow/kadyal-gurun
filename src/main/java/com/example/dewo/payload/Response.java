package com.example.dewo.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

    private String message;
    private Object data;
    private Object user;
    private Object note;
    private Object old;
    private Object update;

    public Response() {
    }

    public Response(Object data) {
        this.data = data;
    }

    public Response(String message, Object data) {
        this.data = data;
        this.message = message;
    }

    public Response(Object user, Object note) {
        this.user = user;
        this.note = note;
    }

    public Response(String detail, Object old, Object update) {
        this.message = detail;
        this.old = old;
        this.update = update;
    }

    public Response(String message) {
        this.message = message;
    }

}
