package com.example.dewo.payload;

import com.example.dewo.model.Article;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseUser {
    private Object user;
    private Object articel;

    public ResponseUser() {
    }

    public ResponseUser(Object user, List<Article> articles) {
        this.user = user;
        this.articel = articles;
    }
}
