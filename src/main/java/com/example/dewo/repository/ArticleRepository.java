package com.example.dewo.repository;

import com.example.dewo.dto.DtoTest;
import com.example.dewo.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

    @Query(value = "SELECT * FROM article WHERE user_id = :userId", nativeQuery = true)
    Article findByUserId(Long userId);

    @Query(value = "SELECT * FROM article WHERE user_id = :userId", nativeQuery = true)
    List<Article> optionalByUserId(Long userId);

    @Query("SELECT NEW com.example.dewo.dto.DtoTest (ar.id, ar.userId, ar.description, ar.title) FROM Article ar")
    List<Object> listWithAlias();

    @Query(value = "SELECT ar.id, ar.userId, ar.description as detail, ar.title as judul FROM Article as ar", nativeQuery = true)
    List<Object> listBambang();

}
