package com.example.dewo.dto;

import com.example.dewo.model.Article;
import com.example.dewo.model.Comment;
import com.example.dewo.model.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoUser {

    private String firstName;
    private String lastName;
    private String address;
    private Integer noHp;
    private DtoArticle article;
    private DtoComment comment;
    private List<Article> articles;

    public DtoUser() {
    }

    public DtoUser(User user) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.address = user.getAddress();
        this.noHp = user.getNoHp();
    }

    public DtoUser(User user, List<Article> articles) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.address = user.getAddress();
        this.noHp = user.getNoHp();
        this.articles = articles;
    }

    public DtoUser(User user, DtoComment dtoComment) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.address = user.getAddress();
        this.noHp = user.getNoHp();
        this.comment = dtoComment;
    }
}
