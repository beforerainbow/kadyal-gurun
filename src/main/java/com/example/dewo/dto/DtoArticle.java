package com.example.dewo.dto;

import com.example.dewo.model.Article;
import com.example.dewo.model.Comment;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoArticle {

    private String title;
    private String description;
    private DtoComment comment;
    private DtoUser user;
    private List<Comment> comments;

    public DtoArticle() {

    }

    public DtoArticle(Article article) {
        this.title = article.getTitle();
        this.description = article.getDescription();
    }

    public DtoArticle(Article article, DtoUser dtoUser) {
        this.title = article.getTitle();
        this.description = article.getDescription();
        this.user = dtoUser;
    }

    public DtoArticle(Article articles, List<Comment> comments) {
        this.title = articles.getTitle();
        this.description = articles.getDescription();
        this.comments = comments;
    }

    public DtoArticle(Article article, DtoComment comment) {
        this.title = article.getTitle();
        this.description = article.getDescription();
        this.comment = comment;
    }

    public DtoArticle(Article article, DtoUser dtoUser, List<Comment> comments) {
        this.title = article.getTitle();
        this.description = article.getDescription();
        this.user = dtoUser;
        this.comments = comments;
    }
}
