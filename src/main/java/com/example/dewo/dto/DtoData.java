package com.example.dewo.dto;

import com.example.dewo.model.Article;
import com.example.dewo.model.Comment;
import com.example.dewo.model.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoData {

    private String title;
    private String description;
    private Long userId;
    private DtoUser users;
    private Article article;
    private List<Article> articles;
    private Comment comment;

    public DtoData() {

    }

    public DtoData(Article article, Comment comment) {
        this.title = article.getTitle();
        this.description = article.getDescription();
        this.comment = comment;

    }

    public DtoData(Article article, DtoUser dtoUser) {
        this.title = article.getTitle();
        this.description = article.getDescription();
        this.userId = article.getUserId();
        this.users = dtoUser;
    }

//    public DtoData(User user, List<DtoArticle> articleList) {
//        this.users = user;
//        this
//    }
}
