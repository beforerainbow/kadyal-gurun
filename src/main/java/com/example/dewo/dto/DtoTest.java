package com.example.dewo.dto;

import lombok.Data;

@Data
public class DtoTest {
    private String detail;
    private String judul;
    private Long id;
    private Long userId;

    public DtoTest(Long id, Long userId, String description, String title) {
        this.detail = description;
        this.judul = title;
        this.id = id;
        this.userId = userId;
    }

    public DtoTest() {

    }
}
