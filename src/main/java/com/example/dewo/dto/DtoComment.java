package com.example.dewo.dto;

import com.example.dewo.model.Comment;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DtoComment {

    private String fieldComment;
    private Long userId;
    private DtoUser user;

    public DtoComment() {

    }

    public DtoComment(Comment comment) {
        this.fieldComment = comment.getFieldComment();
        this.userId = comment.getUserId();
    }

    public DtoComment(Comment comment, DtoUser dtoUser) {
        this.fieldComment = comment.getFieldComment();
        this.user = dtoUser;
    }
}
