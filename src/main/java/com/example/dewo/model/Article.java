package com.example.dewo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "article")
public class Article implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    @JsonProperty
    private String title;

    @Column(name = "description")
    @JsonProperty
    private String description;

    @Column(name = "user_id")
    @JsonProperty
    private Long userId;

}
