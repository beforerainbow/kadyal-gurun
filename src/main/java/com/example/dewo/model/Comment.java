package com.example.dewo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "comment")
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "field_comment")
    @JsonProperty
    private String fieldComment;

    @Column(name = "article_id")
    @JsonProperty
    private Long articleId;

    @Column(name = "user_id")
    @JsonProperty
    private Long userId;

}
